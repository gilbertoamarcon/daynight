#ifndef __PARAMETERS_HPP__
#define __PARAMETERS_HPP__
#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <sstream>
#include <string>

using namespace std;
using namespace cv;

// Parâmetros de execução
#define DISP_PCS	0	// Mostrar cada imagem carregada
#define DISP_PLOT	1	// Plotar espaço RBG no OpenGL

// Parâmetros de arquivo
#define PRE_TRAIN_D	"train/D"	// Local das fotos de treino diurnas
#define PRE_TRAIN_N	"train/N"	// Local das fotos de treino noturnas
#define PRE_TEST_D	"test/D"	// Local das fotos de teste diurnas
#define PRE_TEST_N	"test/N"	// Local das fotos de teste noturnas
#define FORMAT		".jpg"		// Formato do arquivo de imagens
#define PATH		"data"		// Nome do arquivo de resultados

// Parâmetros MLP
#define I	3			// Número de entradas
#define J	1			// Número de neurônios na HL
#define K	1			// Número de saídas
#define N	100000		// Número de épocas
#define P	400			// Número de amostras de treinamento/teste
#define D	0.002		// Tamanho do passo

#define PI				3.14159265359

// Parâmetros de vizualização (OpenGL)
#define VIEW_W			10
#define VIEW_H			10

// Parâmetros de vídeo (OpenGL)
#define WINDOW_TITLE	"Video"
#define FULL_SCREEN		0
#define RENDER_TIME		30
#define WINDOW_H		1280
#define WINDOW_V		720
#define Z_PROX			0.001
#define Z_DIST			1000
#define C_FOVY			75
#define TAM_MOUSE		350
#define FATOR_ZOOM		1.02
#define POINTS			10

// Comandos de Teclado (OpenGL)
#define EXIT 			27
#define MOV_LEFT 		'a'
#define MOV_RIGHT 		'd'
#define MOV_BACK 		's'
#define MOV_FORTH 		'w'
#define MOV_DOWN		'q'
#define MOV_UP 			'e'
#define ZOOM_IN 		'f'
#define ZOOM_OUT 		'v'
#define INC_POINT 		'g'
#define DEC_POINT 		'b'
#define RESET_VIEW_POS	'z'
#define PLOT_AXIS		'x'
#define WALK_SPEED		'c'
#define PLOT_MLP		'r'

// Parametros de movimento da Camera (OpenGL)
#define WALK_VEL 		0.1
#define RUN_VEL 		0.4

#endif
