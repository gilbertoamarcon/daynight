#include "parameters.hpp"
#include "mlp.hpp"
#include "GL/freeglut.h"
#include "GL/gl.h"
#include <stdio.h>

// Objeto MLP
Mlp mlp;

// Conjunto de treinamento
float s[I*P];	// Conjunto de entrada
float d[K*P];	// Conjunto de saída

// Leitura dos dados de treinamento
int readTrainingData();

// Avaliação dos resultados (teste)
int eval();

// Avaliação de uma fotografia, dado seu local (path)
int check(char *path);

// Métodos para vizualização com OpenGL
void movMouse(int x, int y);
void atualiza(int n);
void renderizaCena();
void iniGl();
void teclaPressionada(unsigned char key, int x, int y);
void teclaLiberada(unsigned char key, int x, int y);
void plotEixo();	// Plota o eixo da origem
void plotF();		// Plota os pontos de cada fotografia de treino
void plotMlp();		// Plot o volume de classificação do MLP
void keyPressed(unsigned char key, int x, int y);

// Variaveis de movimento da Camera
float walk_fast		= 0;
int plot_axis		= 1;
int plot_mlp		= 1;
int point_size		= 5;
int zoomOut			= 0;
int zoomIn			= 0;
float point_colors	= 2;
float movL			= 0;
float movR			= 0;
float movB			= 0;
float movF			= 0;
float movU			= 0;
float movD			= 0;
float theta			= 90;
float phi			= 90;

// Variaveis de posicao da Camera
float cam_vel		= 0;
float camX			= 0;
float camY			= 0;	
float camZ			= 0;
float ctrX			= 0.5;
float ctrY			= 0.5;
float ctrZ			= 0.5;
float cam_rad		= 2;
int camera_topo		= 0;
float c_aspect		= (float)WINDOW_H/WINDOW_V;

int main(int argc, char **argv){

	// Leitura dos dados de treinamento
	readTrainingData();

	// Treinamento, com cálculo de tempo
	int taux = (unsigned long)clock();
	mlp.train(s,d);	
	taux = (unsigned long)clock()-taux;

	// Se o usuário forneceu entradas, trate-as
    if(argc == 2)
		check(argv[1]);
	else
		eval();
	
	// Tempo de treinamento
	cout << fixed;
	cout << "t:" << setprecision(6) << setw(12) << (float)taux/CLOCKS_PER_SEC << endl;;

	// Visualização OpenGL
	if(DISP_PLOT){
		glutInit(&argc, argv);
		iniGl();
		glutMainLoop();
	}
	return 0;
}

int check(char *path){

	Mat img;
	vector<Mat> channels;

	// Leitura da imagem
	img = imread(path,1);
	if(!img.data)
		return 0;

	// Cáculo das médias por canal
	split(img,channels);
	float b = mean(channels[0])[0]/255;
	float g = mean(channels[1])[0]/255;
	float r = mean(channels[2])[0]/255;

	// Alimentação do MLP
	mlp.x[0] = r;
	mlp.x[1] = g;
	mlp.x[2] = b;

	// Inferência do MLP
	mlp.eval();

	// Impressão de resultados do MLP
	cout << "R: " << r << " G: " << g << " B: " << b  << " MLP: " << mlp.o[0];
	if(mlp.o[0] > 0.5)
		cout << ": Day" << endl;
	else
		cout << ": Night" << endl;

	// Imagem carregada
	namedWindow("Image",CV_WINDOW_AUTOSIZE);
	imshow("Image",img);

	// Cor média
	Mat avg(128,128,CV_32FC3,Scalar(b,g,r));
	namedWindow("avg",CV_WINDOW_AUTOSIZE);
	imshow("avg",avg);

	waitKey(0);

	return 0;
}

int eval(){
	stringstream ss;

	Mat img;
	vector<Mat> channels;

	int i	= 0;
	int day = 0;
	int p	= 0;
	FILE *fileStream = fopen(PATH,"w+");

	int fp	= 0;
	int fn	= 0;
	int c	= 0;
	while(day < 2){

		// Preparação do endereço da imagem
		ss.str(string());
		if(day)
			ss << PRE_TEST_D << setw(3) << setfill('0') << i++ << FORMAT;
		else
			ss << PRE_TEST_N << setw(3) << setfill('0') << i++ << FORMAT;
		img = imread(ss.str(),1);

		// Leitura da imagem
		if(!img.data){
			i = 0;
			day++;
			continue;
		}

		// Cáculo das médias por canal
		split(img,channels);
		float b = mean(channels[0])[0]/255;
		float g = mean(channels[1])[0]/255;
		float r = mean(channels[2])[0]/255;

		// Alimentação do MLP
		mlp.x[0] = r;
		mlp.x[1] = g;
		mlp.x[2] = b;

		// Inferência do MLP
		mlp.eval();

		// Estatísticas de FP, FN e respostas corretas
		if(mlp.o[0] > 0.5 & !day)
			fp++;
		else if(mlp.o[0] <= 0.5 & day)
			fn++;
		else
			c++;

		// Impressão de resultado da inferência
		cout << ss.str() << " R: " << r << " G: " << g << " B: " << b  << " MLP: " << mlp.o[0] << endl;

		// Resultado salvos em arquivo
		if(day)
			fprintf(fileStream,"1,");
		else
			fprintf(fileStream,"0,");
		fprintf(fileStream,"%d,",	(int)(1e6*r));
		fprintf(fileStream,"%d,",	(int)(1e6*g));
		fprintf(fileStream,"%d,",	(int)(1e6*b));
		fprintf(fileStream,"%d\n",	(int)(1e6*mlp.o[0]));

		// Apresentação das figuras e da média de cor
		if(DISP_PCS){
			namedWindow("Image",CV_WINDOW_AUTOSIZE);
			imshow("Image",img);
			Mat avg(128,128,CV_32FC3,Scalar(b,g,r));
			namedWindow("avg",CV_WINDOW_AUTOSIZE);
			imshow("avg",avg);
			waitKey(0);			
		}
	}
	fclose(fileStream);
	cout << "C: " << c << " FP: " << fp << " FN: " << fn << endl;
	cout << fixed;
	cout << "C: "  << setprecision(2) << 100*(float)c/P << "% FP: " << 100*(float)fp/P << "% FN: " << 100*(float)fn/P << "%" << endl;
	return 0;
}

int readTrainingData(){
	stringstream ss;

	Mat img;
	vector<Mat> channels;

	int i = 0;
	int day = 0;
	int p = 0;
	FILE *fileStream = fopen(PATH,"w+");
	while(day < 2){
		
		// Preparação do endereço da imagem
		ss.str(string());
		if(day)
			ss << PRE_TRAIN_D << setw(3) << setfill('0') << i++ << FORMAT;
		else
			ss << PRE_TRAIN_N << setw(3) << setfill('0') << i++ << FORMAT;
		img = imread(ss.str(),1);

		// Leitura da imagem
		if(!img.data){
			i = 0;
			day++;
			continue;
		}

		// Cáculo das médias por canal
		split(img,channels);
		float b = mean(channels[0])[0]/255;
		float g = mean(channels[1])[0]/255;
		float r = mean(channels[2])[0]/255;

		// Alimentação do conjunto de treinamento do MLP
		s[I*p+0]	= r;
		s[I*p+1]	= g;
		s[I*p+2]	= b;
		d[p++]		= day;

		// Resultado salvos em arquivo
		if(day)
			fprintf(fileStream,"1,");
		else
			fprintf(fileStream,"0,");
		fprintf(fileStream,"%d,",	(int)(1e6*r));
		fprintf(fileStream,"%d,",	(int)(1e6*g));
		fprintf(fileStream,"%d\n",	(int)(1e6*b));

		// Apresentação das figuras e da média de cor
		if(DISP_PCS){
			Mat avg(128,128,CV_32FC3,Scalar(b,g,r));
			namedWindow("Image",CV_WINDOW_AUTOSIZE);
			imshow("Image",img);
			namedWindow("avg",CV_WINDOW_AUTOSIZE);
			imshow("avg",avg);
			waitKey(0);			
		}
	}
	fclose(fileStream);

	return 0;
}

void plotF(){
	glPointSize(point_size);
	glBegin(GL_POINTS);
		for (int p = 0; p < P; p++){
			glColor3f(1.0-d[p],0.0,d[p]);
			glVertex3f(s[I*p+0],s[I*p+1],s[I*p+2]);
		}
	glEnd();
}

void plotMlp(){
	for (int x = 0; x < POINTS; x++){
		for (int y = 0; y < POINTS; y++){
			for (int z = 0; z < POINTS; z++){
				mlp.x[0] = (float)x/POINTS;
				mlp.x[1] = (float)y/POINTS;
				mlp.x[2] = (float)z/POINTS;
				mlp.eval();
				glPushMatrix();
					glColor4f(1.0-mlp.o[0],0.0,mlp.o[0],0.15);
					glTranslatef(mlp.x[0],mlp.x[1],mlp.x[2]);
					glTranslatef(0.5/POINTS,0.5/POINTS,0.5/POINTS);
					glutSolidCube(1.0/POINTS);
				glPopMatrix();
			}
		}
	}
}

void renderizaCena(){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
			gluLookAt(ctrX+camX,ctrY+camY,ctrZ+camZ,ctrX,ctrY,ctrZ, 0,0,camera_topo);

		// Eixos
		if(plot_axis)
			plotEixo();

		// Pontos das fotografias
		plotF();

		// Volume MLP
		if(plot_mlp)
			plotMlp();

	glPopMatrix();
	glutSwapBuffers();
}

void iniGl(){
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(WINDOW_H, WINDOW_V);
	glutCreateWindow(WINDOW_TITLE);
	glutKeyboardFunc(&teclaPressionada);
	glutKeyboardUpFunc(&teclaLiberada);
	glutDisplayFunc(&renderizaCena);
	glutIdleFunc(&renderizaCena);
	glutPassiveMotionFunc(&movMouse);
	glClearColor(0,0,0,0);
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);
	glDisable(GL_ALPHA_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_DST_COLOR);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glutTimerFunc(1,atualiza,0);
	glutSetCursor(GLUT_CURSOR_NONE);
	gluPerspective(C_FOVY,c_aspect,Z_PROX,Z_DIST);
	glutFullScreen();
}

void movMouse(int x, int y){

	int relMouseX = x - WINDOW_H/2;
	int relMouseY = y - WINDOW_V/2;

	if(relMouseX > TAM_MOUSE)
		glutWarpPointer(WINDOW_H/2 - TAM_MOUSE, y);
	else if(relMouseX < - TAM_MOUSE)
		glutWarpPointer(WINDOW_H/2 + TAM_MOUSE, y);

	if(relMouseY > TAM_MOUSE)
		glutWarpPointer(x, WINDOW_V/2 - TAM_MOUSE);
	else if(relMouseY < - TAM_MOUSE)
		glutWarpPointer(x, WINDOW_V/2 + TAM_MOUSE);

	theta	= 180*(double)relMouseX/TAM_MOUSE + 180;
	phi		= 89*(double)relMouseY/TAM_MOUSE + 270;

}

void plotEixo(){
	glColor3f(1,0.5,0.5);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(1,0,0);
	glEnd();
	glColor3f(0.5,1,0.5);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,1,0);
	glEnd();
	glColor3f(0.5,0.5,1);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(0,0,1);
	glEnd();
}

void atualiza(int n){
	glutTimerFunc(1,atualiza,0);
	if(zoomIn)
		cam_rad /= FATOR_ZOOM;
	if(zoomOut)
		cam_rad *= FATOR_ZOOM;
	camX	= cam_rad*sin(PI*theta/180.0)*sin(PI*phi/180.0);
	camY	= cam_rad*cos(PI*theta/180.0)*sin(PI*phi/180.0);
	camZ	= cam_rad*cos(PI*phi/180.0);
	camera_topo	= 2*(phi>180)-1;
	if(movL){
		ctrX -= cam_vel*cos(PI*theta/180.0);
		ctrY += cam_vel*sin(PI*theta/180.0);
	}
	if(movR){
		ctrX += cam_vel*cos(PI*theta/180.0);
		ctrY -= cam_vel*sin(PI*theta/180.0);
	}
	if(movB){
		ctrX -= cam_vel*sin(PI*theta/180.0);
		ctrY -= cam_vel*cos(PI*theta/180.0);
	}
	if(movF){
		ctrX += cam_vel*sin(PI*theta/180.0);
		ctrY += cam_vel*cos(PI*theta/180.0);
	}
	if(movD)
		ctrZ -= cam_vel;
	if(movU)
		ctrZ += cam_vel;
	if(walk_fast)
		cam_vel = RUN_VEL;
	else
		cam_vel = WALK_VEL;
}

void teclaPressionada(unsigned char key, int x, int y){
	switch (key){
		case EXIT:
			exit(0);
			break;
		case WALK_SPEED:
			walk_fast = !walk_fast;
			break;
		case PLOT_AXIS:
			plot_axis = !plot_axis;
			break;
		case PLOT_MLP:
			plot_mlp = !plot_mlp;
			break;
		case RESET_VIEW_POS:
			ctrX = 0;
			ctrY = 0;
			ctrZ = 0;
			break;
		case INC_POINT:
			point_size++;
			break;
		case DEC_POINT:
			point_size--;
			if(point_size < 1)
				point_size = 1;
			break;
		case ZOOM_IN:
			zoomIn = 1;
			break;
		case ZOOM_OUT:
			zoomOut = 1;
			break;
		case MOV_LEFT:
			movL = 1;
			break;
		case MOV_RIGHT:
			movR = 1;
			break;
		case MOV_BACK:
			movB = 1;
			break;
		case MOV_FORTH:
			movF = 1;
			break;
		case MOV_DOWN:
			movU = 1;
			break;
		case MOV_UP:
			movD = 1;
			break;
		default:
		break;
	}
}

void teclaLiberada(unsigned char key, int x, int y){
	switch (key){
		case ZOOM_IN:
			zoomIn = 0;
			break;
		case ZOOM_OUT:
			zoomOut = 0;
			break;
		case MOV_LEFT:
			movL = 0;
			break;
		case MOV_RIGHT:
			movR = 0;
			break;
		case MOV_BACK:
			movB = 0;
			break;
		case MOV_FORTH:
			movF = 0;
			break;
		case MOV_DOWN:
			movU = 0;
			break;
		case MOV_UP:
			movD = 0;
			break;
		default:
			break;
	}
}
