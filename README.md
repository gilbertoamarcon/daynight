# README #

This is a simple application of a multi-layer perception (MLP) Neural Network. 

It identifies whether a  picture loaded was taken during daytime or nighttime, based of previous training.

The MLP was implemented from scratch, encapsulated on its own class file. 

This repository contains 400 pictures training plus a 400 pictures testing data set.

With the present data set, an accuracy of 95% was achieved. 
