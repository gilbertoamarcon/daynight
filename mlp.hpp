#ifndef __MLP_HPP__
#define __MLP_HPP__
#include "parameters.hpp"

class Mlp{
	public:

		// Pesos
		float V[J][I+1];
		float W[K][J+1];

		// Entradas
		float x[I+1];

		// Variáveis Internas
		float u[J];
		float y[J+1];

		float delta_o[K];
		float delta_h[J];

		// Saídas
		float o[K];

		// Construtor
		Mlp();

		// Inicialização dos pesos
		void randomize();

		// Avaliação da saída para dada entrada
		void eval();

		// Processo de treinamento
		void train(float *s,float *d);
};


#endif
