#include "mlp.hpp"

Mlp::Mlp(){
	randomize();
}

void Mlp::randomize(){
	srand(clock());

	// Inicializando pesos V
	for(int i = 0; i < J; i++)
		for(int j = 0; j < I+1; j++)
			V[i][j] = (2*(double)rand()/RAND_MAX - 1);

	// Inicializando pesos W
	for(int i = 0; i < K; i++)
		for(int j = 0; j < J+1; j++)
			W[i][j] = (2*(double)rand()/RAND_MAX - 1);
}

void Mlp::eval(){
	x[I] = 1.0;

	// Computo da saída da HL
	for(int i = 0; i < J; i++){
		u[i] = 0;
		for(int j = 0; j < I+1; j++)
			u[i] += V[i][j]*x[j];
		y[i] = 1.0/(1+exp(-u[i]));
	}

	// Computo da saída da OL
	y[J] = 1;
	for(int i = 0; i < K; i++){
		o[i] = 0;
		for(int j = 0; j < J+1; j++)
			o[i] += W[i][j]*y[j];
	}
}

void Mlp::train(float *s, float *d){
	randomize();

	// Número de épocas
	for(int n = 0; n < N; n++){

		// Imprimindo progresso
		if(!(n%1000))
			cout << "Iteration " << n/1000 << "/" << N/1000 << endl;

		// Elementos do conjunto de treinamento
		for(int p = 0; p < P; p++){

			// Recebimento da entrada p
			for(int i = 0; i < I; i++)
				x[i] = s[p*I+i];

			// Avaliação da saída do MLP
			eval();

			// Erro delta da OL
			for(int k = 0; k < K; k++)
				delta_o[k] = d[p*K+k] - o[k];

			// Erro delta da HL
			for(int j = 0; j < J; j++){
				delta_h[j] = 0;
				for(int k = 0; k < K; k++)
					delta_h[j] += delta_o[k]*W[k][j];
				delta_h[j] *= (1-y[j])*y[j];
			}

			// Iteração dos pesos W
			for(int k = 0; k < K; k++)
				for(int j = 0; j <= J; j++)
					W[k][j] += D*delta_o[k]*y[j];

			// Iteração dos pesos V
			for(int j = 0; j < J; j++)
				for(int i = 0; i <= I; i++)
					V[j][i] += D*delta_h[j]*x[i];
		}
	}
}